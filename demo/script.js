/*
© Copyright 2018-2020 Yves de Champlain, Stéphane Lapointe.

SPDX-License-Identifier: LiLiQ-Rplus-1.1
License-Filename: LICENCE
*/


/* Variables declarations */

// Global variables
var quill_ad;

var tooltips = {
    'clean': 'Clear formatting /\nRemove attributes',
    'image': 'Insert an image',
    'link': 'Insert an URL',
    'simple_comment': 'Comment',
    'video': 'Insert a video',
};


$( document ).ready((event) => {

    quill_ad = new Quill('#editor', {
        modules: {
            toolbar: true,
        },
        theme: 'certification',
        bounds: '#editor'
    });

    // Tooltips for editor_ad buttons
    let showTooltip = (el) => {
        let tool = el.className.replace('ql-', '');
        if (tooltips[tool]) {
            $(el).attr("title", tooltips[tool]);
        }
    };
    let toolbarElement = document.querySelector('.ql-toolbar');
    if (toolbarElement) {
        let matches = toolbarElement.querySelectorAll('button');
        for (let el of matches) {
            showTooltip(el);
        }
    }

    // Dynamic word count
    quill_ad.on('text-change', function () {
        development_changed = true;
        var text = quill_ad.getText();
        text = text.trim();
        $("#counter")[0].textContent = text.split(/\s+/).length + ' words';
        $("#output_delta")[0].value = JSON.stringify(quill_ad.editor.getDelta(), null, 4);
    });

    // Toggle switch
    $("#myonoffswitch").change(function () {
      if ($("#myonoffswitch").is(":checked")){
        $('.ql-simple_comment').attr("class","ql-simple_commentNormal");
      }
      else{
        $('.ql-simple_commentNormal').attr("class","ql-simple_comment");
      }
    });

});
