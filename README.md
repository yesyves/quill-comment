# Quill-Comment
Add comments in quilljs

- Associate a comment with a section of text
- Mark the commented text with a background color
- Record and show the username and date the comment was created

## Details

This module comes from the Passages project:
https://bitbucket.org/yesyves/passages/

This  project was not aiming to create a single comment module but rather many
specialized comments modules for quilljs that would share a common codebase
based on Quill Tooltip and would be fairly easy to customize.

This explains the rather complicated structure as well as the references to
certification, Passages being a portfolio for recognition and certification
of prior experience-based learning. This also explain the many dependencies,
but these are mostly for the looks.

The demo includes a toggle button to show / hide comments tags as well as a
word counter and a Quill ops viewer.

Hope you enjoy !
