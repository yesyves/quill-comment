/*
© Copyright 2018-2020 Yves de Champlain, Stéphane Lapointe.

SPDX-License-Identifier: LiLiQ-Rplus-1.1
License-Filename: LICENCE
*/

/*
 * Creates custom theme overriding snow theme.
 *
 * References;
 *   - https://api.jquery.com/jquery.extend/
 */

const Snow = Quill.import('themes/snow');

import icons from '../ui/icons.js';
import simple_comment_comment from '../modules/simple_comment-comment.js';

var toolbarOptions = [
    ['clean', { 'list': 'ordered' }, { 'list': 'bullet' }],
    ['link', 'image', 'video'],
    // Custom formats
    ['simple_comment'],
];


class CertificationTheme extends Snow {
  constructor(quill, options) {
    if (
      options.modules.toolbar != null &&
      options.modules.toolbar.container == null
    ) {
      options.modules.toolbar.container = toolbarOptions;
    }
    super(quill, options);
    this.quill.container.classList.add('ql-certification');
  }
}

CertificationTheme.DEFAULTS = $.extend(true, {}, Snow.DEFAULTS, {
    modules: {
        simple_comment_comment: true,
    }
});

export default CertificationTheme;
