/*
© Copyright 2018-2020 Yves de Champlain, Stéphane Lapointe.

SPDX-License-Identifier: LiLiQ-Rplus-1.1
License-Filename: LICENCE
*/

import SimpleComment from './formats/simple_comment.js';
import SimpleCommentComment from './modules/simple_comment-comment.js';

import Icons from './ui/icons.js';
import CertificationTheme from './themes/certification.js';


Quill.register(
  {
    'formats/simple_comment': SimpleComment,
    'modules/simple_comment_comment': SimpleCommentComment,
    'ui/icons': Icons, // Overwriting Quill modules/icons
    'themes/certification': CertificationTheme,
  },
  true, // Suppress warning(s) (Default: False)
);
