/*
© Copyright 2018-2020 Yves de Champlain, Stéphane Lapointe.

SPDX-License-Identifier: LiLiQ-Rplus-1.1
License-Filename: LICENCE
*/

class Range {
  constructor(index, length = 0) {
    this.index = index;
    this.length = length;
  }
}

export default Range;
