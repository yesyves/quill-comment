/*
© Copyright 2018-2020 Yves de Champlain, Stéphane Lapointe.

SPDX-License-Identifier: LiLiQ-Rplus-1.1
License-Filename: LICENCE
*/

import CertificationTag from './certification-tag.js'

class SimpleComment extends CertificationTag {
  // Creates corresponding DOM node
  static create(value) {
    // NOTE: if saving the comment, value is the content of the textarea
    // Make the difference between json string and simple string
    let comment, date, author = null;
    if (typeof value === 'string' && value.search('ql-simple_comment') > 0) {
      let values = JSON.parse(value);
      comment = values.attributes.comment || '';
      date = values.attributes.date || '';
      author = values.attributes.author || '';
    } else {
      comment = value
    }
    const node = super.create({
      'id': Date.now(),
      'type': 'simple_comment',
      'class': 'ql-simple_comment',
      'attributes': {
        'comment': comment,
        'date': date || Date.now(),
        'author': username
      }
    });
    return node;
  }

  static getAttributes(domNode) {
    const simple_comment_tag = this.getDataValue(domNode);
    return simple_comment_tag.attributes;
  };

  static getComment(domNode) {
    const simple_comment_tag = this.getDataValue(domNode);
    return simple_comment_tag.attributes.comment;
  }

  static getAuthor(domNode) {
    const simple_comment_tag = this.getDataValue(domNode);
    return simple_comment_tag.attributes.author;
  }

  static getDate(domNode) {
    const simple_comment_tag = this.getDataValue(domNode);
    return simple_comment_tag.attributes.date;
  }

  static getDataValue(domNode) {
    return JSON.parse(domNode.getAttribute('data-value'));
  }
}

SimpleComment.blotName = 'simple_comment';
SimpleComment.tagName = 'SPAN';
SimpleComment.className = 'ql-simple_comment';

export default SimpleComment;
