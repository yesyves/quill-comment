/*
© Copyright 2018-2020 Yves de Champlain, Stéphane Lapointe.

SPDX-License-Identifier: LiLiQ-Rplus-1.1
License-Filename: LICENCE
*/

const Tooltip = Quill.import('ui/tooltip');

import SimpleCommentBlot from '../formats/simple_comment.js';
import Range from '../utilities/range.js';

/**
 * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date/toLocaleDateString
 * @type {{month: string, year: string, weekday: string, day: string}}
 */
let dateFormat = {
  // weekday: 'long',
  year: 'numeric',
  month: 'long',
  day: 'numeric'
}
// try to get the navigator's language or else set 'undefined' as default (should automatically use browser locale)
// `navigator.language` is always supposed to be available, so quite reliable
let lang = navigator.language || undefined;

class CommentTooltip extends Tooltip {
  constructor(quill, boundsContainer) {
    super(quill, boundsContainer);
    // Ajouter le textarea
    this.textarea = this.root.querySelector('textarea');
    // Add HTML DOM event listeners
    this.listen();
    // Get HTML DOM element ql-preview to later interact with
    // [Log] <span class="ql-preview"></span> (simple_comment-tooltip.js, line 18) ??? YD
    this.preview = this.root.querySelector('span.ql-preview');
    this.comment_author = this.root.querySelector('div > span.ql-psg-comment-user');
    this.comment_date = this.root.querySelector('div > span.ql-psg-comment-date');
    this.root.setAttribute('data-mode', 'simple_comment');
  }

  // Add HTML DOM event listeners
  listen() {
    // (base)
    // Add escape key to textarea
    this.textarea.addEventListener('keydown', event => {
      if (event.key === 'Escape') {
        this.cancel();
        event.preventDefault();
      }
    });
    // (snow)
    // Add event to ql-action button click.
    // The behavior (Edit/Save) is based on tooltip class ql-editing
    this.root.querySelector('a.ql-action').addEventListener('click', event => {
      // if (this.root.classList.contains('ql-editing')) {
      //   this.save();
      // } else {
      //   this.edit(this.textarea.value);
      // }
      this.save();
      event.preventDefault();
    });

    // Remove format at user’s current selection
    this.root.querySelector('a.ql-remove').addEventListener('click', event => {
      if (this.range != null) {
        const range = this.range;
        this.restoreFocus();
        this.quill.formatText(range, 'simple_comment', false, Quill.sources.USER);
        delete this.range;
      }
      event.preventDefault();
      this.hide();
    });

    // https://quilljs.com/docs/api/#selection-change
    this.quill.on(
      'selection-change',
      (range, oldRange, source) => {
        if (range == null) {
            this.preview.textContent = '';
            return;
        }
        // There is no selection, only the cursor inside the editor
        if (range.length === 0 && source === Quill.sources.USER) {
          const [blot, offset] = this.quill.scroll.descendant(
            SimpleCommentBlot,
            range.index,
          );
          if (blot != null) {
            this.range = new Range(range.index - offset, blot.length());
            // const preview = SimpleCommentBlot.formats(blot.domNode);
            const attributes = SimpleCommentBlot.getAttributes(blot.domNode);
            // TODO : Remplacer le textarea par sa valeur dans show() YD
            // TODO : Faire suivre les 2 valeurs de show() à edit() YD
            // this.preview.textContent = preview;
            // this.show();
            this.edit(attributes);
            this.position(this.quill.getBounds(this.range));
            return;
          }
        } else {
          delete this.range;
        }
        this.hide();
      },
    );
  }

  // Event triggered when the escape key is pressed while the tooltip has focus
  cancel() {
    this.hide();
    this.restoreFocus();
  }

  edit(attributes = null) {
    // Make the HTML DOM element
    this.root.classList.remove('ql-hidden');
    this.root.classList.add('ql-editing');
    // Ajoute le texte s'il y en a déjà
    if (attributes != null && attributes.comment != null && attributes.comment !== "") {
      this.textarea.value = attributes.comment;
      this.root.classList.remove('ql-editing');
    } else {
      this.textarea.value = '';
    }
    // Fix position relative to selected bolt in the editor
    this.position(this.quill.getBounds(this.quill.selection.savedRange));
    this.textarea.select();

    // display the comment's author and date
    if (attributes != null) {
      this.comment_author.textContent = attributes.author;
      this.comment_date.textContent = new Date(attributes.date).toLocaleDateString(lang, dateFormat);
    }
    // set default values when creating a new comment
    else {
      this.comment_author.textContent = username;
      this.comment_date.textContent = new Date(Date.now()).toLocaleDateString(lang, dateFormat);
    }
  }

  restoreFocus() {
    const { scrollTop } = this.quill.scrollingContainer;
    this.quill.focus();
    this.quill.scrollingContainer.scrollTop = scrollTop;
  }


  save() {
    let { value } = this.textarea;

    const { scrollTop } = this.quill.root;
    if (this.range) {
      // When created
      this.quill.formatText(
        this.range,
        'simple_comment',
        value,
        Quill.sources.USER,
      );
      delete this.range;
    } else {
      // When edited
      this.restoreFocus();
      this.quill.format('simple_comment', value, Quill.sources.USER);
    }
    this.quill.root.scrollTop = scrollTop;

    this.textarea.value = '';

    this.hide();
  }

  // Show tooltip without editing functionality
  show() {
    super.show();
  }
}

// HTML DOM tooltip
// SPECS
// span = user name (TODO)
// textarea = explanation
CommentTooltip.TEMPLATE = [
  '<p><span class="ql-preview"></span></p>',
  '<div class="mb-1 flex">' +
    '<div class="flex-auto">' +
      '<i class="fas fa-user pr-2 blue-text"></i>' +
      '<span class="ql-psg-comment-user mr-3"></span>' +
    '</div>' +
    '<div class="flex-auto text-right">' +
      '<i class="fas fa-clock pr-2 blue-text"></i>' +
      '<span class="ql-psg-comment-date"></span>' +
    '</div>' +
  '</div>',
  '<textarea rows="4" cols="40"></textarea>',
  '<br/>',
  '<a class="ql-action"></a>',
  '<a class="ql-remove"></a>',
].join('');

export default CommentTooltip;
